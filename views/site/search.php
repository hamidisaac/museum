<!-- Page Header-->
<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">نتایج جستجو</p>
        </div>
    </section>

</header>
<!-- Contact information-->
<section class="section section-wrap section-reverse bg-white">
    <div class="section-wrap-inner section-lg">
        <div class="shell shell-bigger">
            <div class="range range-50">
                <div class="cell-md-12 cell-xl-5 text-md-left">
                    <div class="section-wrap-content" style="direction: rtl;text-align: right; max-width: 1100px;">
                        <h3>نتایج جستجو</h3>
                        <div class="divider divider-default"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
