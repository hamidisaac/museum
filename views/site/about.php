<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">درباره ما</p>
        </div>
    </section>

</header>


<!-- A few words about us-->
<section dir="rtl" class="section section-lg bg-white">
    <div class="shell shell-bigger">
        <div class="range range-ten range-50 range-sm-center range-md-middle range-lg-justify">
            <div class="cell-sm-9 cell-md-5 cell-lg-4">
                <h3>درباره خزانه جواهرات ملی</h3>
                <div class="divider divider-default"></div>
                <p class="heading-5">گنجينه بی‌مانند « خزانه جواهرات ملی » مجموعه‌ای از گرانبهاترين جواهرات جهان است که طی قرون و اعصار بدين صورت فراهم آمده است.</p>
                <p class="text-spacing-sm">هر قطعه از اين جواهرات، گويای بخشی از تاريخ پرفراز و نشيب ملت بزرگ ايران و مبين ذوق و خلاقيت‌های هنری مردم اين مرز و بوم است که خاطرات تلخ و شيرين شکست‌ها، پيروزی‌ها، غرور و خودنمايی‌ها و فرمانفرمايی همراه با زور و نخوت زمامداران گذشته را تداعی می‌کند.</p>
                <p class="text-spacing-sm">شما بازديد کننده عزيزی که به تماشای نفايس و گوهرهای اين مجموعه بی‌نظير می‌نشينيد، قبل از اينکه محو تلألو خيره‌کننده آن شويد، به علل تاريخی و چگونگی جمع‌آوری اين جواهرات بينديشيد و به مصداق « فاعتبروا يا اولی‌الابصار » قضاوت تاريخ و وجدان انسانهای آگاه و آزاده را درمورد گردآورندگان اين جواهرات دريابيد و اهداف آنان را از جمع‌آوری اين مجموعه بشناسيد.</p>
                <p class="text-spacing-sm">گنجينه حاضر از يک طرف بيانگر فرهنگ و تاريخ کهن‌سال و پرتلاطم ملت ايران و گويای داستان بخشی از زندگی پرماجرای گذشتگان و از طرفی ديگر بازگوی قصه اشک‌های خاموش مردم ستمديده و جور کشيده است که نتيجه خودآرايی و فخر‌فروشی قدرتمندان و صاحبان زور در دوران گذشته بوده است.</p>
                <p class="text-spacing-sm">انگيزه ما از ارائه اين جواهرات، شناخت بيشتر فرهنگ و تاريخ ايران و پندگرفتن از عاقبت زورمداران و زراندوزان تاريخ است. با اين هدف، مجموعه حاضر که به ما سپرده شده است در معرض ديد و قضاوت شما قرار می‌گيرد.</p>
                <p class="text-spacing-sm">اهميت جواهرات موجود در "خزانه جواهرات ملی" به ارزش اقتصادی آن محدود نمی‌گردد، بلکه نمايانگر ذوق و سليقه صنعتگران و هنرمندان ايران در دوره‌های مختلف تاريخ و به عنوان ميراث تاريخی، هنری، نماينده هنرهای مستظرفه کشور پهناور ما است.</p>
                <p class="text-spacing-sm">اين جواهرات و نوادر برای زمامداران و حاکمان در طول تاريخ حکم زينت و زيور را داشته و اغلب شکوه و جلال دربار را نشان می‌داده است، ليکن پشتوانه قدرت و ذخيره خزانه مملکتی نيز محسوب می‌شده است.</p>
            </div>
            <div class="cell-sm-9 cell-md-5"><img src="images/daryaye_noor.jpg" alt="" width="720" height="459"/>
            </div>
        </div>
    </div>
</section>

<!-- Our history-->
<section dir="rtl" class="section section-lg bg-white">
    <div class="shell shell-bigger">
        <div class="range range-ten range-50 range-xs-center range-lg-justify">
            <div class="cell-xs-9 cell-sm-12 cell-lg-6">
                <h3>تاريخچه</h3>
                <div class="divider divider-default"></div>
                <p class="text-gray-light">اطلاعات دقيقی در مورد کيفيت و کميت گنجينه جواهرات تا قبل از دوران صفويه در دست نيست و می‌توان گفت تاريخچه جواهرات ايران از زمان سلاطين صفوی آغاز می‌شود. نحوه گردآوری و پديد آوردن مجموعه فعلی را به طور اختصار می‌توان چنين بيان داشت:</p>
                <div class="time-line-vertical inset-sm" style="text-align:right">
                    <div class="time-line-vertical-element">
                        <div class="unit unit-sm unit-sm-horizontal unit-spacing-xxl">
                            <div class="unit-left">
                                <div class="time-line-time">
                                    <time class="wow fadeInLeft" data-wow-delay=".1s" datetime="2017">سال ۱۱۶۰ ﻫ. ق</time>
                                </div>
                            </div>
                            <div class="unit-body">
                                <div class="time-line-content wow fadeInRight" data-wow-delay=".6s">
                                    <h5>تا قبل از صفويه</h5>
                                    <p>تا قبل از صفويه، اقلامی از جواهرات در خزانه دولتی وجود داشته است و براساس نوشته‌های سياحان خارجی (ژان باتيست تاورنيه، شواليه شاردن، برادران شرلی، وارنيگ و ديگران) سلاطين صفوی حدود قرن (907 تا 1148 ﻫ. ق.) شروع به جمع‌آوری نفايس و گوهرها نموده و حتی کارشناسان دولت صفوی جواهرات را از بازارهای هند، عثمانی و کشورهای اروپايی مانند فرانسه و ايتاليا خريداری کرده و به اصفهان، پايتخت حکومت می‌آورده‌اند.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="time-line-vertical-element">
                        <div class="unit unit-sm-horizontal unit-spacing-xxl">
                            <div class="unit-left">
                                <div class="time-line-time">
                                    <time class="wow fadeInLeft" data-wow-delay=".1s" datetime="2017">سال ۱۱۶۰ ﻫ. ق</time>
                                </div>
                            </div>
                            <div class="unit-body">
                                <div class="time-line-content wow fadeInRight" data-wow-delay=".7s">
                                    <h5>در سال ۱۱۶۰ ﻫ. ق. و پس از قتل نادر</h5>
                                    <p>در سال ۱۱۶۰ ﻫ. ق. و پس از قتل نادر، احمد بيک افغان ابدالی از سرداران نادر، دست به غارت جواهرات خزانه نادر زد. از جمله اين گوهرها که از ايران خارج شد و هرگز بازنگشت، الماس معروف "کوه نور" بود. اين الماس به دست احمد شاه درانی و رنجيت سينگ پنجابی افتاد. با شکست رنجيت سينگ از انگليس‌ها، "کوه نور" به چنگ کمپانی هند شرقی افتاد و در ۱۲۶۶ ﻫ. ق. / ۱۸/۵۰ م. به ملکه ويکتوريا اهداء شد.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="time-line-vertical-element">
                        <div class="unit unit-sm-horizontal unit-spacing-xxl">
                            <div class="unit-left">
                                <div class="time-line-time">
                                    <time class="wow fadeInLeft" data-wow-delay=".1s" datetime="2017">سال ۱۱۶۰ ﻫ. ق</time>
                                </div>
                            </div>
                            <div class="unit-body">
                                <div class="time-line-content wow fadeInRight" data-wow-delay=".8s">
                                    <h5> پس تا زمان قاجاريه</h5>
                                    <p>از آن پس تا زمان قاجاريه، باقيمانده خزائن تغييرات چندانی نيافت. در دوران قاجاريه، مجموعه جواهرات، جمع‌آوری و ضبط شد و تعدادی از جواهرات به تاج کيانی، تخت نادری، کره جواهر نشان و تخت طاووس (تخت خورشيد) نصب گرديد.</p>
                                    <p>دو گوهر ديگر که به تدريج به مجموعه اضافه شد، يکی فيروزه که از سنگ‌های قيمتی ايرانی است و از معادن نيشابور استخراج می‌گردد و ديگری مرواريد که از خليج فارس صيد می‌شد.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell-xs-9 cell-sm-12 cell-lg-3">
                <div class="range range-30">
                    <div class="cell-sm-4 cell-lg-12 wow fadeInUp" data-wow-delay=".1s"><img src="images/01.jpg" alt="" width="420" height="280"/>
                    </div>
                    <div class="cell-sm-4 cell-lg-12 wow fadeInUp" data-wow-delay=".2s"><img src="images/02.jpg" alt="" width="420" height="280"/>
                    </div>
                    <div class="cell-sm-4 cell-lg-12 wow fadeInUp" data-wow-delay=".3s"><img src="images/03.jpg" alt="" width="420" height="280"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>