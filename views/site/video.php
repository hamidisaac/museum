<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">ویدیو</p>
        </div>
    </section>

</header>

<section dir="rtl" class="section section-lg bg-white">
    <div class="shell shell-bigger">
        <div class="range range-ten range-50 range-sm-center range-md-middle range-lg-justify">
                <?php
                if(Yii::$app->user->identity->credit > 100) {
                    ?>
                    <div class="cell-sm-6 cell-md-6 cell-lg-4">
                        <h3>درباره خزانه جواهرات ملی</h3>
                        <div class="divider divider-default"></div>
                        <p class="heading-5">گنجينه بی‌مانند « خزانه جواهرات ملی » مجموعه‌ای از گرانبهاترين جواهرات جهان است که طی قرون و اعصار بدين صورت فراهم آمده است.</p>
                        <p class="text-spacing-sm">هر قطعه از اين جواهرات، گويای بخشی از تاريخ پرفراز و نشيب ملت بزرگ ايران و مبين ذوق و خلاقيت‌های هنری مردم اين مرز و بوم است که خاطرات تلخ و شيرين شکست‌ها، پيروزی‌ها، غرور و خودنمايی‌ها و فرمانفرمايی همراه با زور و نخوت زمامداران گذشته را تداعی می‌کند.</p>
                    </div>
                    <div class="cell-sm-6 cell-md-6 cell-lg-6">
                        <video poster="images/daryaye_noor.jpg" width="720" height="459" id="player" playsinline controls>
                            <source src="videos/100.webm" type="video/webm"/>
                        </video>
                    </div>
                    <?php
                } else {
                    echo '<div class="cell-sm-6 cell-md-6 cell-lg-6">';
                    echo '<h2>اعتبار شما : '.Yii::$app->user->identity->credit.'</h2>';
                    echo '<h4>اعتبار کافی برای دیدن این محتوا را ندارید، برای افزایش اعتبار خود اقدام نمایید</h4>';
                    echo '</div>';
                }
                ?>
        </div>
    </div>
</section>

