<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">گالری</p>
        </div>
    </section>

</header>
<section class="section section-lg text-center bg-gray-lighter">
    <div class="shell-wide">
        <h3>گالری</h3>
        <div class="divider divider-default"></div>
        <div class="isotope-wrap range range-0">
            <div class="cell-lg-12">
                <div class="isotope isotope-default" data-isotope-layout="masonry" data-isotope-group="gallery-01" data-lightgallery="group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 1"><a class="gallery-item" href="images/daryaye_noor.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/02.jpg" alt="" width="570" height="790"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">دریای نور</p>
                                        <p class="caption-text">این سنگ با زوج خود الماس کوه نوربه سال 1739 در زمره ی غنایم وارده نادر شاه ازهندوستان به ایران آمده است دوره های بعد از نادر شاه کوه نوربه هندوستان بازگشت وازآن پس به انگلستان رفت وهنوز در جواهرات سلطنتی این کشوراست اما دریای نور در ایران باقی ماند.این الماس تردیدی نیست که از معادن گلکده(جنوب هند)به دست آمده است رنگ آن مایل به صورتی و بزرگترین الماس صورتی رنگ جهان است.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 1"><a class="gallery-item" href="images/03.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/03.jpg" alt="" width="570" height="380"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">تاج کیانی</p>
                                        <p class="caption-text">این تاج دیهیم سلطنت خاندان قاجار بوده است و در تاج گذاری رضا شاه از آن استفاده شد.این تاج در اصل از چهار تکه ساخته شده است که عبارتست از یک سربند یک بلاط(گونه ای از الماس تراشیده شده)مروارید چند کنگره و یک مخمل </p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 1"><a class="gallery-item" href="images/04.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/04.jpg" alt="" width="570" height="790"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">الماس نورالعین</p>
                                        <p class="caption-text">درشت ترین برلیان پشت گلی سند گذاری شده ی جهان سنگ اصلی این نیمتاج است این تاج توسط هری وینستون به مناسبت ازدواج محمد رضا پهلوی و فرح ساخته شده است امکان آن است که این سنگ همراه الماس دریای نور روزگاری الماس کرسی اعظم تاورنیه را تشکیل میداده است.این الماس اکنون در نگین دانی از پلاتین قرار دارد.دو سوی آن به الماس های زرد صورتی و بی رنگ که تراش برلیان و اشکال گوناگون دارداحاطه شده است.زیر آن ردیفی موجدار از الماس های شاخکی به کار رفته است.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 2"><a class="gallery-item" href="images/05.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/05.jpg" alt="" width="570" height="790"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">جقه ی نادری</p>
                                        <p class="caption-text">زیور ظریفی که به نام جقه ی نادری معروف است تخمه ی آن زمردی درشت و بسیار اعلاست که وزن آن به 65 قیراط میرسد.پهلو و زیر این سنگ شبیهی است از توپ-نیزه-کوس-و درفش که با شبیه نواری به هم بسته و همه به الماس مرصع است.مایه رزمی ای که در این جقه است و در مجموعه های سلطنتی هیچ کجا به چشم نمی آید،برازنده ی شاه رزمجویی است که کشور خود را به آن عزم و قدرت گسترد چه بسا که نام جقه ی نادری از این طرح الهام گرفته است.اما به دلایلی دور از ذهن به نظر می رسد که نادر شاه این جقه را به کار برده باشد.اولا این زیور به صورت کنونی غیر قابل استفاده بوده است</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 2"><a class="gallery-item" href="images/06.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/06.jpg" alt="" width="570" height="380"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">نیمتاج فتحعلیشاه</p>
                                        <p class="caption-text">این نیمتاج سنگین و زیبا را برای فتحعلی شاه ساخته بودند و شاه چون در بیشتر مواقع کلاهی بلند از پوست بره به سر داشت،این نیمتاج را در پیشانیگاه خود جای می داد.در میان تصویر های عمده و مینیاتوری  که از فتحعلیشاه به جاست،در چند مورد به این نیمتاج بر میخوریم.نیمتاج از لعل سرخ یاقوت و الماس تعبیه شده است که در زه های زر سوار است و بر زمینه ای از مینای نیم شفاف سبز قرار دارد.</p>
                                    </div>
                                </div></a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 isotope-item" data-filter="type 2"><a class="gallery-item" href="images/08.jpg" data-lightgallery="item">
                                <div class="gallery-item-image">
                                    <figure><img src="images/08.jpg" alt="" width="570" height="380"/>
                                    </figure>
                                    <div class="caption">
                                        <p class="caption-title">تاج کیانی</p>
                                        <p class="caption-text">این تاج دیهیم سلطنت خاندان قاجار بوده است و در تاج گذاری رضا شاه از آن استفاده شد.این تاج در اصل از چهار تکه ساخته شده است که عبارتست از یک سربند یک بلاط(گونه ای از الماس تراشیده شده)مروارید چند کنگره و یک مخمل </p>
                                    </div>
                                </div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
