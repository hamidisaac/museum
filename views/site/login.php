<!-- Page Header-->
<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">ورود</p>
        </div>
    </section>

</header>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'ورود';
$this->params['breadcrumbs'][] = $this->title;
?>
<section dir="rtl" class="section section-lg bg-white">
    <div class="shell shell-bigger">
        <div class="range range-ten range-50 range-sm-center range-md-middle range-lg-justify">
            <div class="cell-sm-9 cell-md-5 cell-lg-8">
                <h1><?= Html::encode($this->title) ?></h1>

                <br/>

                <div class="col-lg-offset-1" style="color:#999;">
                    با این مشخصات می توانید وارد شوید <strong>admin/admin</strong> یا <strong>demo/demo</strong>.<br>
                </div>

                <br/>

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "<div class=\"col-lg-7\">{error}</div>\n<div class=\"col-lg-3\">{input}</div>\n{label}",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->checkbox([
                        'template' => "<div class=\"col-lg-7\">{error}</div>\n<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n",
                    ]) ?>

                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11">
                            <?= Html::submitButton('ورود', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>

                <br/>
                <br/>
        </div>
    </div>
</div>

</section>
