<div class="swiper-container swiper-slider swiper-modern" data-loop="true" data-autoplay="5000" data-simulate-touch="false">
    <div class="swiper-wrapper">
        <div class="swiper-slide" data-slide-bg="images/10.jpg">
            <div class="swiper-slide-caption">
                <div class="section-lg context-dark text-center">
                    <div class="shell shell-wide">
                        <div class="range range-xs-center">
                            <div class="cell-xl-10">
                                <div class="heading-decor" data-caption-animate="fxRotateInDown" data-caption-delay="550"><span>گرانبهاترین جواهرات سلطنتی دنیا</span></div>
                                <h2 data-caption-animate="fxRotateInDown" data-caption-delay="550">به برترین موزه و گنجینه ی تاریخ خوش آمدید</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="images/Slider01.jpg">
            <div class="swiper-slide-caption">
                <div class="section-lg context-dark text-center">
                    <div class="shell shell-wide">
                        <div class="range range-xs-center">
                            <div class="cell-xl-10">
                                <div class="heading-decor" data-caption-animate="fxRotateInLeft" data-caption-delay="550"><span>گرانبهاترین جواهرات سلطنتی دنیا</span></div>
                                <h2 data-caption-animate="fxRotateInDown" data-caption-delay="550">به برترین موزه و گنجینه ی تاریخ خوش آمدید</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="images/Slider02.jpg">
            <div class="swiper-slide-caption">
                <div class="section-lg context-dark text-center">
                    <div class="shell shell-wide">
                        <div class="range range-xs-center">
                            <div class="cell-xl-10">
                                <div class="heading-decor" data-caption-animate="fxRotateInUp" data-caption-delay="550"><span>گرانبهاترین جواهرات سلطنتی دنیا</span></div>
                                <h2 data-caption-animate="fxRotateInDown" data-caption-delay="550">به برترین موزه و گنجینه ی تاریخ خوش آمدید</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="images/Slider03.jpg">
            <div class="swiper-slide-caption">
                <div class="section-lg context-dark text-center">
                    <div class="shell shell-wide">
                        <div class="range range-xs-center">
                            <div class="cell-xl-10">
                                <div class="heading-decor" data-caption-animate="fxRotateIn" data-caption-delay="550"><span>گرانبهاترین جواهرات سلطنتی دنیا</span></div>
                                <h2 data-caption-animate="fxRotateInDown" data-caption-delay="550">به برترین موزه و گنجینه ی تاریخ خوش آمدید</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Swiper controls-->
    <div class="swiper-pagination-wrap">
        <div class="swiper-pagination"></div>
    </div>
</div>

<!-- A few words about us-->
<section dir="rtl" class="section section-lg section-2-columns bg-white">
    <div class="shell shell-bigger">
        <div class="range range-ten range-50 range-sm-center range-md-middle range-lg-justify">
            <div class="cell-sm-9 cell-md-5 cell-xl-4">
                <h3>درباره خزانه جواهرات ملی</h3>
                <div class="divider divider-default"></div>
                <p class="heading-5">گنجينه بی‌مانند « خزانه جواهرات ملی » مجموعه‌ای از گرانبهاترين جواهرات جهان است که طی قرون و اعصار بدين صورت فراهم آمده است.</p>
                <p class="text-spacing-sm">
                    هر قطعه از اين جواهرات، گويای بخشی از تاريخ پرفراز و نشيب ملت بزرگ ايران و مبين ذوق و خلاقيت‌های هنری مردم اين مرز و بوم است که خاطرات تلخ و شيرين شکست‌ها، پيروزی‌ها، غرور و خودنمايی‌ها و فرمانفرمايی همراه با زور و نخوت زمامداران گذشته را تداعی می‌کند.
                </p>
                <p class="text-spacing-sm">
                    شما بازديد کننده عزيزی که به تماشای نفايس و گوهرهای اين مجموعه بی‌نظير می‌نشينيد، قبل از اينکه محو تلألو خيره‌کننده آن شويد، به علل تاريخی و چگونگی جمع‌آوری اين جواهرات بينديشيد و به مصداق « فاعتبروا يا اولی‌الابصار » قضاوت تاريخ و وجدان انسانهای آگاه و آزاده را درمورد گردآورندگان اين جواهرات دريابيد و اهداف آنان را از جمع‌آوری اين مجموعه بشناسيد.
                </p>
                <p class="text-spacing-sm">
                    گنجينه حاضر از يک طرف بيانگر فرهنگ و تاريخ کهن‌سال و پرتلاطم ملت ايران و گويای داستان بخشی از زندگی پرماجرای گذشتگان و از طرفی ديگر بازگوی قصه اشک‌های خاموش مردم ستمديده و جور کشيده است که نتيجه خودآرايی و فخر‌فروشی قدرتمندان و صاحبان زور در دوران گذشته بوده است.
                </p>
                <p class="text-spacing-sm">
                    انگيزه ما از ارائه اين جواهرات، شناخت بيشتر فرهنگ و تاريخ ايران و پندگرفتن از عاقبت زورمداران و زراندوزان تاريخ است. با اين هدف، مجموعه حاضر که به ما سپرده شده است در معرض ديد و قضاوت شما قرار می‌گيرد.
                </p>
                <p class="text-spacing-sm">
                    اهميت جواهرات موجود در "خزانه جواهرات ملی" به ارزش اقتصادی آن محدود نمی‌گردد، بلکه نمايانگر ذوق و سليقه صنعتگران و هنرمندان ايران در دوره‌های مختلف تاريخ و به عنوان ميراث تاريخی، هنری، نماينده هنرهای مستظرفه کشور پهناور ما است.
                </p>
                <p class="text-spacing-sm">
                    اين جواهرات و نوادر برای زمامداران و حاکمان در طول تاريخ حکم زينت و زيور را داشته و اغلب شکوه و جلال دربار را نشان می‌داده است، ليکن پشتوانه قدرت و ذخيره خزانه مملکتی نيز محسوب می‌شده است.
                </p>
                <a class="button button-default-outline" href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/about">بیشتر</a>
            </div>
            <div class="cell-sm-9 cell-md-5"><img src="images/daryaye_noor.jpg" alt="" width="720" height="459"/>
            </div>
        </div>
    </div>
</section>

<section class="section section-lg bg-gray-dark text-center">
    <div class="shell shell-wide pricing-shell-wrap">
        <h3>پذيرش</h3>
        <div class="divider divider-default"></div>
        <div dir="rtl" class="range range-xs-center range-lg range-30">
            <!-- Pricing Box-->
            <div class="cell-sm-6 cell-lg-3">
                <div class="pricing-box pricing-box-novi">
                    <div class="pricing-box-header">
                        <h4 class="text-spacing-md"></h4>
                    </div>
                    <div class="pricing-box-body">
                        <ul class="pricing-box-list">
                            <li>کاربران با هر بار شارژ مالی ، یک بار می توانند به صورت مجازی از سایت دیدن کنند.<br><br> </li>
                        </ul>
                        <div class="pricing-box-price">
                            <div class="heading-4"></div>
                        </div><a class="button button-sm button-secondary" href="#">خرید</a>
                    </div>
                </div>
            </div>
            <!-- Pricing Box-->
            <div class="cell-sm-6 cell-lg-3">
                <div class="pricing-box pricing-box-novi">
                    <div class="pricing-box-header">
                        <h4 class="text-spacing-md"></h4>
                    </div>
                    <div class="pricing-box-body">
                        <ul class="pricing-box-list">
                            <li>کاربران برای تماشای دموی سایت میبایست مبلغ ۲ دلار و برای تماشای مجموعه کامل جواهرات سلطنتی مبلغ ۵ دلار  پرداخت کنند.</li>
                        </ul>
                        <div class="pricing-box-price">
                            <div class="heading-4"></div>
                        </div><a class="button button-sm button-secondary" href="#">خرید</a>
                    </div>
                </div>
            </div>
            <!-- Pricing Box-->
            <div class="cell-sm-6 cell-lg-3">
                <div class="pricing-box pricing-box-novi">
                    <div class="pricing-box-header">
                        <h4 class="text-spacing-md"></h4>
                    </div>
                    <div class="pricing-box-body">
                        <ul class="pricing-box-list">
                            <li>پنل سفارشی ۱<br><br><br><br></li>
                        </ul>
                        <div class="pricing-box-price">
                            <div class="heading-4"></div>
                        </div><a class="button button-sm button-secondary" href="#">خرید</a>
                    </div>
                </div>
            </div>
            <!-- Pricing Box-->
            <div class="cell-sm-6 cell-lg-3">
                <div class="pricing-box pricing-box-novi">
                    <div class="pricing-box-header">
                        <h4 class="text-spacing-md"></h4>
                    </div>
                    <div class="pricing-box-body">
                        <ul class="pricing-box-list">
                            <li>پنل سفارشی ۲<br><br><br><br></li>
                        </ul>
                        <div class="pricing-box-price">
                            <div class="heading-4"></div>
                        </div><a class="button button-sm button-secondary" href="#">خرید</a>
                    </div>
                </div>
            </div>
            <div>
                <p>شارژ حساب فقط از طریق بیت کوین</p>
                <p>برای دریافت کیف پول بیت کوین <a href="http://coin.base.com" target="_blank">اینجا</a> را کلیک کنید</p>
                <p>جهت دریافت کیف پول رایگان <a href="#" target="_blank">اینجا</a> را کلیک کنید</p>
            </div>
        </div>
    </div>
</section>

<!-- Blog posts-->
<section class="section section-lg bg-gray-lighter text-center">
    <div class="shell-wide">
        <div class="range">
            <div class="cell-xs-12">
                <h3>بلاگ</h3>
                <div class="divider divider-default"></div>
            </div>
        </div>
        <div dir="rtl" class="range range-30 range-xs-center">
            <div class="cell-sm-6 cell-lg-5 cell-xl-6">
                <article class="post-blog-large" style="direction: rtl">
                    <figure class="post-blog-large-image"><img src="images/taj.jpg" alt="تاج کیانی" width="868" height="640"/>
                    </figure>
                    <ul class="post-blog-meta">
                        <li><span>توسط</span>&nbsp;<a href="#">مدیر</a></li>
                    </ul>
                    <div class="post-blog-large-caption" style="text-align: right">
                        <ul class="post-blog-tags">
                        </ul><a class="post-blog-large-title" href="#">تاج کیانی</a>
                        <p class="post-blog-large-text">تاج کیانی، تاج پادشاهی در دورهٔ قاجار است که به دستور فتحعلی شاه ساخته شد و مورد استفاده پادشاهان پس از وی قرار گرفت. با فروپاشی ساسانیان، شاهان ایران دیگر هیچگاه از تاج به این شکل استفاده ننموده بودند و در تمام آن دوران تاج به صورت جقه بوده‌است. عناصر به کار رفته در این تاج الماس، زمرد، یاقوت و مروارید می‌باشد. تاج کیانی هم‌اکنون در موزه جواهرات ملی ایران نگهداری می‌شود.</p>
                        <a class="button button-xs button-secondary" href="#">ادامه مطلب</a>
                    </div>
                </article>
            </div>
            <div class="cell-sm-6 cell-lg-5 cell-xl-6">
                <article class="post-blog-large" style="direction: rtl">
                    <figure class="post-blog-large-image"><img src="images/09.jpg" alt="تاج کیانی" width="868" height="640"/>
                    </figure>
                    <ul class="post-blog-meta">
                        <li><span>توسط</span>&nbsp;<a href="#">مدیر</a></li>
                    </ul>
                    <div class="post-blog-large-caption" style="text-align: right">
                        <ul class="post-blog-tags">
                        </ul><a class="post-blog-large-title" href="#">کمربند زرین</a>
                        <p class="post-blog-large-text">کمربند زرین یکی از اشیاء تاریخی در موزه جواهرات ملی است.</p>
                        <p class="post-blog-large-text">یک قطعه زمرد منحصر بفرد به وزن حدود یکصد و هفتاد و شش قیراط که با شصت قطعه الماس برلیان و ۱۴۵ قطعه الماس فلامک تزئین شده است، اساس این کمربند را تشکیل داده است. طول بند زربافت آن ۱۱۹ سانتیمتر بوده و به دستور ناصرالدین شاه ساخته شده است.</p>
                        <a class="button button-xs button-secondary" href="#">ادامه مطلب</a>
                    </div>
                </article>
            </div>
        </div><a class="button button-secondary" href="#">نمایش همه پست ها</a>
    </div>
</section>

<!--<section class="section section-lg bg-white">
    <div class="shell shell-wide shell-box-minimal-wrap">
        <div class="range text-center">
            <div class="cell-xs-12">
                <h3>additional services</h3>
                <div class="divider divider-default"></div>
            </div>
        </div>
        <div class="range range-50 range-xs-center text-left">
            <div class="cell-xs-10 cell-sm-6 cell-lg-4">
                <article class="box-minimal box-minimal-border">
                    <div class="box-minimal-icon mdi mdi-bus"></div>
                    <p class="big box-minimal-title">Local Bus Service</p>
                    <hr>
                    <div class="box-minimal-text text-spacing-sm">We offer free Bus Service for children and adults, which allows young visitors and their parents to quickly reach our museum to usefully spend their time. After visiting us, you will be driven back home in our comfortable buses.</div>
                </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-lg-4">
                <article class="box-minimal box-minimal-border">
                    <div class="box-minimal-icon mdi mdi-camera"></div>
                    <p class="big box-minimal-title">Commercial Photography</p>
                    <hr>
                    <div class="box-minimal-text text-spacing-sm">If you are a fan of history or just like what you see at our museum you can use our Commercial Photography services. They are very affordable and as a result you get high-quality photos by email. It’s a lot better than usual photos!</div>
                </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-lg-4">
                <article class="box-minimal box-minimal-border">
                    <div class="box-minimal-icon mdi mdi-nature-people"></div>
                    <p class="big box-minimal-title">Outdoor Playground</p>
                    <hr>
                    <div class="box-minimal-text text-spacing-sm">Our museum also has lots of interesting things for children including our Outdoor Playground, which is mainly our entertainment center for everyone up to 15 y.o. Our  team will provide your chilren with interesting activities.</div>
                </article>
            </div>
        </div>
    </div>
</section>-->

