<!-- Page Header-->
<header class="section page-header breadcrumbs-custom-wrap bg-gray-dark breadcrumbs-background-01">
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom breadcrumbs-custom-svg">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom-title">تماس با ما</p>
        </div>
    </section>

</header>
<!-- Contact information-->
<section class="section section-wrap section-reverse bg-white">
    <div class="section-wrap-inner section-lg">
        <div class="shell shell-bigger">
            <div class="range range-50">
                <div class="cell-md-12 cell-xl-5 text-md-left">
                    <div class="section-wrap-content" style="direction: rtl;text-align: right; max-width: 1100px;">
                        <h3>اطلاعات تماس</h3>
                        <div class="divider divider-default"></div>
                        <p class="text-spacing-sm">بازديد از خزانه جواهرات ملی روزهای شنبه تا سه‌شنبه از ساعت 14:00 الی 16:30 امکان‌پذير است.
                            خزانه جواهرات ملی در غير از ساعات فوق و نيز روزهای چهارشنبه، پنج‌شنبه، جمعه و تعطيلات رسمی، تعطيل می‌باشد.</p>
                        <ul class="list-xs list-darker">
                            <li class="box-inline"><span class="icon icon-md-smaller icon-secondary mdi mdi-map-marker"></span>
                                <div><a class="text-spacing-sm" href="#">تهران،میدان امام خمینی، خيابان فردوسی، نرسیده به چهارراه استانبول،
                                        ساختمان بانک مرکزی جمهوری اسلامی ايران
                                        خزانه جواهرات ملی</a></div>
                            </li>
                            <li class="box-inline"><span class="icon icon-md-smaller icon-secondary mdi mdi-phone"></span>
                                <ul class="list-comma">
                                    <li><a class="text-spacing-sm" href="callto:#">تلفن: 64463785-64463792-64463799-64463030
                                        </a></li>
                                </ul>
                            </li>
                            <li class="box-inline"><span class="icon icon-md-smaller icon-secondary mdi mdi-email-open"></span>
                                <a href="#">javaheratmeli@gmail.com</a>
                            </li>
                            <li class="box-inline"><span class="icon icon-md-smaller icon-secondary mdi mdi-email-open"></span>
                                <a href="#">javahetmeli@yahoo.com</a>
                            </li>
                            <li class="box-inline"><span class="icon icon-md-smaller icon-secondary mdi mdi-web"></span>
                                <a href="https://www.example.com">https://www.example.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
