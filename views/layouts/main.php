<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets -->
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;">
        <a href="http://windows.microsoft.com/en-US/internet-explorer/">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
        </a>
    </div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
<!-- Page preloader-->
<div class="page-loader">
    <div class="page-loader-body">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"> </div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"> </div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"> </div>
                </div>
            </div>
            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page-->
<div class="page">
    <!-- Page Header-->
    <header class="section page-header bg-gray-dark breadcrumbs-custom-wrap">
        <div class="rd-navbar-wrap rd-navbar-default">
            <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-lg-device-layout="rd-navbar-fullwidth" data-md-stick-up-offset="2px" data-lg-stick-up-offset="2px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
                <div class="rd-navbar-inner">
                    <!-- RD Navbar Panel-->
                    <div class="rd-navbar-panel">
                        <!-- RD Navbar Toggle-->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                        <!-- RD Navbar Brand-->
                        <!--<div class="rd-navbar-brand"><a class="brand-name" href="index.html"><img class="logo-default" src="images/logo-default-197x70.png" alt="" width="197" height="70"/><img class="logo-inverse" src="images/logo-inverse-197x70.png" alt="" width="197" height="70"/></a></div>-->
                        <div class="rd-navbar-aside-right-inner">
                            <!-- RD Navbar Search-->
                            <div class="rd-navbar-search"><a class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search" href="#"><span></span></a>
                                <form class="rd-search" action="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/search" method="POST">
                                    <div class="form-wrap">
                                        <label class="form-label form-label" for="rd-navbar-search-form-input" style="direction: rtl;right:25px">جستجو ...</label>
                                        <input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" style="direction: rtl;">
                                        <div class="rd-search-results-live" id="rd-search-results-live"> </div>
                                    </div>
                                    <button class="rd-search-form-submit mdi mdi-magnify"></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="rd-navbar-aside-right">
                        <div class="rd-navbar-nav-wrap">
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <!--<li><a href="#">Services</a>
                                    <ul class="rd-navbar-dropdown">
                                        <li><a href="services.html">Services</a>
                                        </li>
                                        <li><a href="single-service.html">Single Service</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li><a href="#">Gallery</a>
                                    <ul class="rd-navbar-dropdown">
                                        <li><a href="grid-album-gallery.html">Grid Album Gallery</a>
                                        </li>
                                        <li><a href="fullwidth-gallery-inside-title.html">Fullwidth Gallery Inside Title</a>
                                        </li>
                                        <li><a href="grid-gallery-outside-title.html">Grid Gallery Outside Title</a>
                                        </li>
                                        <li><a href="masonry-gallery-outside-title.html">Masonry Gallery Outside Title</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li><a href="#">Blog</a>
                                    <ul class="rd-navbar-dropdown">
                                        <li><a href="classic-blog.html">Classic Blog</a>
                                        </li>
                                        <li><a href="grid-blog.html">Grid Blog</a>
                                        </li>
                                        <li><a href="masonry-blog.html">Masonry Blog</a>
                                        </li>
                                        <li><a href="modern-blog.html">Modern Blog</a>
                                        </li>
                                        <li><a href="audio-post.html">Audio Post</a>
                                        </li>
                                        <li><a href="image-post.html">Image Post</a>
                                        </li>
                                        <li><a href="single-post.html">Single Post</a>
                                        </li>
                                        <li><a href="video-post.html">Video Post</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li><a href="#">Pages</a>
                                    <ul class="rd-navbar-megamenu rd-navbar-megamenu-banner">
                                        <li><img src="images/image-7-570x380.jpg" alt="" width="570" height="380"/>
                                        </li>
                                        <li>
                                            <ul class="rd-megamenu-list">
                                                <li><a href="404-page.html">404 Page</a></li>
                                                <li><a href="503-page.html">503 Page</a></li>
                                                <li><a href="careers.html">Careers</a></li>
                                                <li><a href="single-job.html">Single Job</a></li>
                                                <li><a href="coming-soon.html">Coming Soon</a></li>
                                                <li><a href="pricing.html">Pricing</a></li>
                                                <li><a href="tooltips.html">Tooltips</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <ul class="rd-megamenu-list">
                                                <li><a href="our-history.html">Our History</a></li>
                                                <li><a href="login-page.html">Login Page</a></li>
                                                <li><a href="registration-page.html">Registration Page</a></li>
                                                <li><a href="search-results.html">Search Results</a></li>
                                                <li><a href="under-construction.html">Under Construction</a></li>
                                                <li><a href="privacy-policy.html">Privacy policy</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <ul class="rd-megamenu-list">
                                                <li><a href="accordions.html">Accordions</a></li>
                                                <li><a href="countdown.html">Countdown</a></li>
                                                <li><a href="forms.html">Forms</a></li>
                                                <li><a href="grid-system.html">Grid System</a></li>
                                                <li><a href="tables.html">Tables</a></li>
                                                <li><a href="tabs.html">Tabs</a></li>
                                                <li><a href="typography.html">Typography</a></li>
                                                <li><a href="radials.html">Radials</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>-->
                                <?php
                                    if (!Yii::$app->user->isGuest) {
                                        echo '<li><a href="'.Yii::$app->getUrlManager()->getBaseUrl().'/index.php?r=site/logout">خروج</a></li>';
                                    } else {
                                        echo '<li><a href="'.Yii::$app->getUrlManager()->getBaseUrl().'/index.php?r=site/login">ورود</a></li>';
                                    }
                                ?>
                                <li><a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/contact">تماس با ما</a></li>
                                <li><a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/about">درباره ما</a></li>
                                <li><a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/video">ویدیو</a>
                                <li><a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/gallery">گالری</a>
                                <li><a href="<?php echo Yii::$app->getUrlManager()->getBaseUrl(); ?>/index.php?r=site/index">خانه</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <?= $content ?>
    <!-- Page Footer-->
    <!-- Footer Minimal -->
    <footer class="section page-footer page-footer-minimal text-center bg-gray-dark">
        <div class="shell shell-wide">
            <div class="range range-xs-center range-sm-middle range-30">
                <div class="cell-sm-10 cell-md-7 cell-lg-3 text-lg-left">
                </div>
                <div class="cell-sm-10 cell-md-7 cell-lg-6">
                    <p class="right">&#169;&nbsp;<span class="copyright-year"></span> کلیه حقوق این وب‌سایت متعلق به موزه خزانه جواهرات ملی می‌باشد </p>
                </div>
                <div class="cell-sm-10 cell-md-7 cell-lg-3 text-lg-right">
                    <ul class="group-xs group-middle">
                        <li><a class="icon icon-md-middle icon-circle icon-secondary-5-filled-type-2 mdi mdi-twitter" href="#"></a></li>
                        <li><a class="icon icon-md-middle icon-circle icon-secondary-5-filled-type-2 mdi mdi-instagram" href="#"></a></li>
                        <li><a class="icon icon-md-middle icon-circle icon-secondary-5-filled-type-2 mdi mdi-telegram" href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- PANEL-->
<!-- END PANEL-->
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"> </div>
<!-- Javascript-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>